package com.javapartice.InputOutput;

import java.io.*;

public class FileOperations {
    static String filepath = "src/Files/";

    public void createFile(String fileName) throws IOException {
        File file = new File(filepath + fileName);
        if(file.createNewFile()){
            System.out.println("New File Created");
        }
        else{
            System.out.println("File is already exists");
        }
    }

    public void editFile(String fileName, String data) throws IOException {
        BufferedWriter bw = null;
        try{
            bw = new BufferedWriter(new FileWriter(filepath + fileName, true));
            bw.write(data);
            System.out.println("data written to file successfully");
        }
        catch(Exception e){
            System.err.println(e);
        }
        finally {
            if(bw != null){
                bw.close();
            }
        }
    }

    public void viewFile(String fileName) throws IOException {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(filepath+fileName));
            int i;
            while((i=br.read())!=-1)
                System.out.print((char)i);
        }
        catch (Exception e){
            System.err.println(e);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        System.out.println();
    }

    public void deleteFile(String fileName){
        File file = new File(filepath+fileName);
        if(file.delete()){
            System.out.println("File deleted successfully");
        }
        else{
            System.out.println("File not found");
        }
    }

    public static void main(String[] args) throws IOException {
        FileOperations fileOperations = new FileOperations();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            System.out.println("1. Create a new File");
            System.out.println("2. Edit File");
            System.out.println("3. View File");
            System.out.println("4. Delete File");
            System.out.println("5. Exit");
            System.out.print("Enter your option : ");
            int option = Integer.parseInt(br.readLine());
            if(option == 5){
                break;
            }
            String fileName;
            switch (option){
                case 1:
                    System.out.print("Please enter file name to create : ");
                    fileName = br.readLine();
                    fileOperations.createFile(fileName);
                    break;
                case 2:
                    System.out.print("Enter the file name to Edit : ");
                    fileName = br.readLine();
                    System.out.print("Enter the data to write into the file : ");
                    String data = br.readLine();
                    fileOperations.editFile(fileName, data);
                    break;
                case 3:
                    System.out.print("Enter the File name to view : ");
                    fileName = br.readLine();
                    fileOperations.viewFile(fileName);
                    break;
                case 4:
                    System.out.print("Enter the File name to delete : ");
                    fileName = br.readLine();
                    fileOperations.deleteFile(fileName);
                default:
                    System.out.println("Please enter valid number");
            }
        }
    }
}
