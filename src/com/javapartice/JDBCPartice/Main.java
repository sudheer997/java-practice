package com.javapartice.JDBCPartice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException, SQLException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        EmployeeManagement employeeManagement = new EmployeeManagement();
        Map employeeIdMap;
        int employeeId;
        while (true){
            System.out.println("1. Create record");
            System.out.println("2. view record");
            System.out.println("3. update record");
            System.out.println("4. Delete record");
            System.out.println("5. Exit");
            System.out.print("Enter choice : ");
            int choice = Integer.parseInt(br.readLine());
            if(choice == 5){
                break;
            }
            switch (choice){
                case 1:
                    System.out.print("Enter employee first name : ");
                    String firstName = br.readLine();
                    System.out.print("Enter employee last name : ");
                    String lastName = br.readLine();
                    System.out.print("Enter phone number");
                    long phoneNumber = Long.parseLong(br.readLine());
                    System.out.print("Enter Gender : ");
                    String gender = br.readLine();
                    System.out.print("Enter email id : ");
                    String emailId = br.readLine();
                    employeeManagement.showSkills();
                    System.out.print("Choose skill id : ");
                    int skillId = Integer.parseInt(br.readLine());
                    employeeManagement.showProjects();
                    System.out.print("Choose project id : ");
                    int projectId = Integer.parseInt(br.readLine());
                    employeeManagement.insertRecord(firstName, lastName, emailId, phoneNumber, gender, skillId, projectId);
                    break;
                case 2:
//                    view record
                    employeeManagement.viewRecord();
                    break;
                case 3:
                    employeeIdMap = employeeManagement.viewRecord();
                    System.out.print("Enter choice : ");
                    employeeId = (int) employeeIdMap.getOrDefault(Integer.parseInt(br.readLine()), -1);
                    System.out.println(employeeId);
                    if(employeeId == -1){
                        break;
                    }
                    System.out.println("1. First name");
                    System.out.println("2. Last name");
                    System.out.println("3. Domain");
                    System.out.println("4. Email");
                    System.out.println("5. project name");
                    System.out.print("Enter your option : ");
                    int option = Integer.parseInt(br.readLine());
                    switch (option){
                        case 1:
                            System.out.print("Enter your new First name to change : ");
                            employeeManagement.updatePersonalDetails(employeeId, br.readLine(), "first_name");
                            break;
                        case 2:
                            System.out.print("Enter your new last name to change : ");
                            employeeManagement.updatePersonalDetails(employeeId, br.readLine(), "last_name");
                            break;
                        case 3:
                            employeeManagement.showSkills();
                            System.out.println("Choose new Domain : ");
                            int domainId = Integer.parseInt(br.readLine());
                            employeeManagement.updateDomain(employeeId, domainId);
                            break;
                        case 4:
                            System.out.print(" Enter new Email id : ");
                            employeeManagement.updateEmailId(employeeId, br.readLine());
                            break;
                        case 5:
                            employeeManagement.showProjects();
                            System.out.print("Enter new project id : ");
                            int projectid = Integer.parseInt(br.readLine());
                            employeeManagement.updateProjectDetails(employeeId, projectid);
                            break;
                        default:
                            System.out.println("Invalid option");
                    }
                    break;
                case 4:
                    employeeIdMap = employeeManagement.viewRecord();
                    System.out.print("Enter choice : ");
                    employeeId = (int) employeeIdMap.getOrDefault(Integer.parseInt(br.readLine()), -1);
                    if(employeeId == -1) {
                        System.out.print("Please enter valid number");
                        break;
                    }
                    employeeManagement.deleteRecord(employeeId);
                    break;
                default:
                    System.out.println("Please enter valid number");
            }
        }

    }
}

