package com.javapartice.Collections.Histories;

import com.javapartice.Collections.Carts.Cart;

import java.util.Date;

public class History {
    private Cart cart;
    private String date;

    History(Cart cart, String date){
        this.cart = cart;
        this.date = date;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String toString(){
        return "Product name : " + cart.getProduct().getProductName() + " Quantity : " + cart.getCount() + " cost : " +
                cart.getProduct().getCost() + " Cart value : " + cart.getCartValue() + " Date of purchase : " + date;
    }
}
