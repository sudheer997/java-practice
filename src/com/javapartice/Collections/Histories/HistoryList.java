package com.javapartice.Collections.Histories;

import com.javapartice.Collections.Carts.Cart;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HistoryList {
    public HashMap<String, List<History>> historyMap = new HashMap<>();

    List<History> historyList;

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(String userName) {
        this.historyList = historyMap.getOrDefault(userName, new ArrayList<>());
    }

    public void addToHistory(String userName, Cart cart){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        historyList.add(new History(cart, dtf.format(now)));
        historyMap.put(userName, historyList);
        System.out.println("Item check out successfully");
    }

    public void showHistory(){
        if(historyList.isEmpty()){
            System.out.println("No History available");
            return;
        }
        System.out.println("Items in History are :");
        System.out.println("------------------------------------------------------");
        for (History cartItem: historyList) {
            System.out.println(cartItem);
            System.out.println("------------------------------------------------------");
        }
    }
}
