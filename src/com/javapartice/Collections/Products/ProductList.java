package com.javapartice.Collections.Products;

import java.util.HashMap;
import java.util.Map;

public class ProductList {

    public HashMap<Integer, Product> products = new HashMap<>();
    static int productItemCount = 0;

    public void showProducts(){
        if(products.isEmpty()){
            System.out.println("No products available in the store");
            return;
        }
        System.out.println("Products in store are : ");
        System.out.println("------------------------------------------------------");
        for(Map.Entry product : products.entrySet()){
            System.out.println(product.getValue());
            System.out.println("------------------------------------------------------");
        }
    }

    public void addProduct(String productName, float cost, int quantity){
        products.put(++productItemCount, new Product(productName, cost, productItemCount, quantity));
        System.out.println(" Product added to the store successfully");
    }

    public Product getProduct(int productNumber){
        return products.getOrDefault(productNumber, null);
    }
}
