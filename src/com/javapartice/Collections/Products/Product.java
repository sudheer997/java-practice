package com.javapartice.Collections.Products;

public class Product {
    private String productName;
    private float cost;
    final private int productNumber;
    private int quantity;

    public Product(String productName, float cost, int productNumber, int quantity) {
        this.productName = productName;
        this.cost = cost;
        this.productNumber = productNumber;
        this.quantity = quantity;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getProductNumber() {
        return productNumber;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString(){
        return " product number : " + getProductNumber() + " product name:  " + getProductName() + " cost : " + getCost() + " quantity : " + getQuantity();
    }
}
