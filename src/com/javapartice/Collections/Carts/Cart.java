package com.javapartice.Collections.Carts;

import com.javapartice.Collections.Products.Product;

import java.util.Objects;

public class Cart {
    private Product product;
    private int count;
    final private int cartNumber;
    private float cartValue ;

    public Cart(Product product, int count, int cartNumber) {
        this.product = product;
        this.count = count;
        this.cartNumber = cartNumber;
        this.cartValue = count * Objects.requireNonNull(product).getCost();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCartNumber() {
        return cartNumber;
    }

    public float getCartValue() {
        cartValue = count * Objects.requireNonNull(product).getCost();
        return cartValue;
    }

    public String toString(){
        return "Cart ID : " + getCartNumber() + " product name : " + product.getProductName() + " cost : " + product.getCost() +
                " count : " + getCount() +" Cart Value : " + getCartValue();
    }
}
