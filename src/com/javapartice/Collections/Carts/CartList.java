package com.javapartice.Collections.Carts;

import com.javapartice.Collections.Products.Product;

import java.util.HashMap;
import java.util.Map;

public class CartList {
    public HashMap<String, Map<Integer, Cart>> cartHistory = new HashMap<>();
    static int cartItemCount = 0;

    Map<Integer, Cart> cartList;

    public Map<Integer, Cart> getCartList() {
        return cartList;
    }

    public void setCartList(String userName) {
        this.cartList = cartHistory.getOrDefault(userName, new HashMap<>());
    }


    public void viewCart(){
        if(cartList.isEmpty()){
            System.out.println("Cart is Empty");
            return;
        }
        System.out.println("Items in cart are : ");
        System.out.println("------------------------------------------------------");
        for(Map.Entry<Integer, Cart> cartItem : cartList.entrySet()){
            System.out.println(cartItem.getValue());
            System.out.println("------------------------------------------------------");
        }
    }

    public void addToCart(String userName, Product product, int count){
//        check whether given product is present in cart or not?
        boolean itemStatus = false;
        for(Map.Entry<Integer, Cart> cartItem : cartList.entrySet()){
            Cart cart = (Cart) cartItem.getValue();
            if(cart.getProduct().equals(product)){
                cart.setCount(cart.getCount() + count);
                itemStatus = true;
            }
        }
        if(!itemStatus){
            cartList.put(++cartItemCount, new Cart(product, count, cartItemCount));
        }
        cartHistory.put(userName, cartList);
        System.out.println("Item Added to cart successfully");
    }

    public Cart getCart(int cartId){
        return cartList.getOrDefault(cartId, null);
    }

    public void removeCart(int cartId){
        cartList.remove(cartId);
    }
}
