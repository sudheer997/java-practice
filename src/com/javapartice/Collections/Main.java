package com.javapartice.Collections;

import com.javapartice.Collections.Carts.Cart;
import com.javapartice.Collections.Carts.CartList;
import com.javapartice.Collections.Histories.HistoryList;
import com.javapartice.Collections.Products.Product;
import com.javapartice.Collections.Products.ProductList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Users users = new Users();
        users.createUsers();
        ProductList productList = new ProductList();
        CartList cartList = new CartList();
        HistoryList history = new HistoryList();
        System.out.println("Welcome to Ecommerce website");
        while (true){
            System.out.println("Enter Your login credentials");
            System.out.print("User Name : ");
            String userName = br.readLine();
            System.out.print("Password : ");
            String password = br.readLine();
            boolean validUser = users.validateUser(userName, password);
            if(!validUser){
                System.out.println("Please enter valid credentials");
                continue;
            }
            int option;
            if(userName.equals("Admin")){
                while (true){
                    System.out.println("1. Show Products");
                    System.out.println("2. Add products");
                    System.out.println("3. logout");
                    System.out.print("Enter your choice : ");
                    option = Integer.parseInt(br.readLine());
                    if(option == 3){
                        break;
                    }
                    switch (option){
                        case 1:
//                            view products
                            productList.showProducts();
                            break;
                        case 2:
//                            Add products
                            System.out.print("Enter product name : ");
                            String productName = br.readLine();
                            System.out.print("Enter Cost of the product: ");
                            float cost = Float.parseFloat(br.readLine());
                            System.out.print("Number of quantity : ");
                            int quantity = Integer.parseInt(br.readLine());
                            productList.addProduct(productName, cost, quantity);
                            break;
                        default:
                            System.out.println("Please enter valid number");
                    }
                }
            }
            else{
                while (true){
                    cartList.setCartList(userName);
                    history.setHistoryList(userName);
                    System.out.println("1. show Products");
                    System.out.println("2. Add products to cart");
                    System.out.println("3. view cart");
                    System.out.println("4. check out");
                    System.out.println("5. Order History");
                    System.out.println("6. Logout");
                    System.out.print("Enter your choice : ");
                    option = Integer.parseInt(br.readLine());
                    if(option == 6){
                        break;
                    }
                    switch (option){
                        case 1:
                            productList.showProducts();
                            break;
                        case 2:
//                            Add product to the cart
                            productList.showProducts();
                            System.out.print("Select product from the product list by using product number :");
                            int productNumber = Integer.parseInt(br.readLine());
                            Product product = productList.getProduct(productNumber);
                            if(product == null){
                                System.out.println("Please enter valid product number");
                                break;
                            }
                            System.out.print("Number of items need to add : ");
                            int count = Integer.parseInt(br.readLine());
                            if(count <= product.getQuantity()){
                                product.setQuantity(product.getQuantity() - count);
                                cartList.addToCart(userName, product, count);
                            }
                            else{
                                System.out.println("Out of Stock");
                            }
                            break;
                        case 3:
                            cartList.viewCart();
                            break;
                        case 4:
                            cartList.viewCart();
                            System.out.print("Enter cart id to check out :");
                            int cartId = Integer.parseInt(br.readLine());
                            Cart cart = cartList.getCart(cartId);
                            if(cart == null){
                                System.out.println("Please enter valid cart ID");
                                break;
                            }
                            cartList.removeCart(cartId);
                            history.addToHistory(userName, cart);
                            break;
                        case 5:
                            history.showHistory();
                            break;
                        default:
                            System.out.println("Please choose valid option");

                    }
                }
            }
        }
    }
}
