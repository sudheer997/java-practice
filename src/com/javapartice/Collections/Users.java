package com.javapartice.Collections;

import java.util.HashMap;

public class Users {
    HashMap<String, String> users = new HashMap<String, String>();
    public void createUsers(){
        users.put("Admin", "Admin");
        users.put("user", "user");
        users.put("user1", "user1");
    }

    public boolean validateUser(String userName, String password){
        if(users.containsKey(userName)){
            return users.get(userName).equals(password);
        }
        else{
            return false;
        }
    }
}
