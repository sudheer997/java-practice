package com.javapartice.MultiThreads;


class Resource{
    synchronized void method1(Resource resource2){
        System.out.println("Thread1 - Begins");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        resource2.method2(this);
        System.out.println("Thread1 - Ends");

    }

    synchronized void method2(Resource resource1){
        System.out.println("Thread2 - Begins");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        resource1.method1(this);
        System.out.println("Thread2 - Ends");
    }
}

class Thread1 extends Thread {
    private Resource s1;
    private Resource s2;

    public Thread1(Resource s1, Resource s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public void run() {
        s1.method1(s2);
    }
}


class Thread2 extends Thread
{
    private Resource s1;
    private Resource s2;

    public Thread2(Resource s1, Resource s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public void run() {
        s2.method2(s1);
    }
}

public class DeadLockExample extends Thread{
    public static void main(String[] args) {
        Resource resource1 = new Resource();
        Resource resource2 = new Resource();

        Thread1 t1 = new Thread1(resource1, resource2);
        t1.start();

        // creating second thread and starting it
        Thread2 t2 = new Thread2(resource1, resource2);
        t2.start();

    }
}
