package com.javapartice.stringpartice;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StringsPartice1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the String : ");
        String inputString = br.readLine();
        while (true){
            System.out.println("choose operations listed below");
            System.out.println("1. Get the number of words in a given string");
            System.out.println("2. Count number of chars or substring in a given string");
            System.out.println("3. Replace the all characters in a given string");
            System.out.println("4. Insert the particular String after particular string or char in a given string");
            System.out.println("5. check palindrome or not");
            System.out.println("6. View String");
            System.out.println("7. Exit");
            System.out.print("Please choose options from below options : ");
            int option = Integer.parseInt(br.readLine());
            if(option == 7){
                break;
            }
            switch (option){
                case 1 :
                    if(inputString.isEmpty()){
                        System.out.print("Number of words in a given string is " + 0);
                        break;
                    }
                    System.out.println(inputString.split(" ").length);
                    break;
                case 2:
                    System.out.print("Enter a character or string to find count : ");
                    String searchString = br.readLine();
                    int count = inputString.split(searchString).length - 1;
                    if(count == 0){
                        System.out.println("Given String is not found");
                    }
                    else{
                        System.out.println("Count is " + count);
                    }
                    break;
                case 3:
                    System.out.print("Enter a string or character to replace in a given string : ");
                    String replaceStringFrom = br.readLine();
                    System.out.print("Enter replace character : ");
                    String replaceStringTo = br.readLine();
                    inputString = inputString.replaceAll(replaceStringFrom, replaceStringTo);
                    System.out.println(inputString);
                    break;
                case 4:
                    System.out.print("Enter the insert string : ");
                    String insertString = br.readLine();
                    System.out.print("before which character or string we need insert ");
                    String posString = br.readLine();
                    int index = inputString.indexOf(posString);
                    if(index == -1){
                        System.out.println("Enter substring is not found");
                    }
                    else{
                        StringBuilder mutableInput = new StringBuilder(inputString);
                        inputString = String.valueOf(mutableInput.insert(index , insertString));
                        System.out.println(inputString);
                    }
                    break;
                case 5:
                    StringBuilder input1 = new StringBuilder(inputString);
                    if(inputString.equalsIgnoreCase(String.valueOf(input1.reverse()))){
                        System.out.println("Given String is a palindrome");
                    }
                    else{
                        System.out.println("Given string is not a palindrome");
                    }
                    break;
                case 6:
                    System.out.println(inputString);
                default:
                    System.out.println("Enter valid number");
                    break;
            }
        }
    }
}
