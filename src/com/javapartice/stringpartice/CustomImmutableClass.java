package com.javapartice.stringpartice;

public final class CustomImmutableClass {
    final private int id ;

    public int getId() {
        return id;
    }

    public CustomImmutableClass(int id) {
        this.id = id;
    }
}
