package com.javapartice.oopsconcept;

import java.util.Arrays;
import java.util.Comparator;
public class PhoneBook implements ContactBook{
    Contact[] contacts = new Contact[100];
    static int noOfContacts = 0;

    @Override
    public void createContact(String name, long phoneNumber, String email) {
        if(validateName(name) && validatePhoneNumber(phoneNumber)) {
            Contact contact = new Contact();
            contact.setName(name);
            contact.setPhoneNumber(phoneNumber);
            contact.setEmail(email);
            contacts[noOfContacts] = contact;
            ++noOfContacts;
        }
    }

    @Override
    public void modifyContact(String oldName, String newName, long phoneNumber, String email){
        for (short i = 0; i < noOfContacts ; i++) {
            Contact contact = contacts[i];
            if (contact.getName() != null && contact.getName().equalsIgnoreCase(oldName)) {
                if(validateName(newName) && validatePhoneNumber(phoneNumber)) {
                    contact.setName(newName);
                    contact.setPhoneNumber(phoneNumber);
                    contact.setEmail(email);
                }
                break;
            }
        }
    }

    @Override
    public void showContacts() {
        for (short i = 0; i < noOfContacts; i++) {
            Contact contact = contacts[i];
            if(contact.getName() == null){
                continue;
            }
            System.out.println("Name : " + contact.getName() + " phone number : " + contact.getPhoneNumber() +
                    " Email : " + contact.getEmail());
        }
    }

    @Override
    public void showContacts(String name){
        boolean flag = false;
        for (short i = 0; i < noOfContacts; i++) {
            Contact contact = contacts[i];
            if(contact.getName() != null && contact.getName().equalsIgnoreCase(name)){
                System.out.println("Name : " + contact.getName() + " phone number : " + contact.getPhoneNumber() +
                        " Email : " + contact.getEmail());
                flag = true;
            }
        }
        if(! flag){
            System.out.println("Details not found");
        }
    }

    @Override
    public void deleteContact(String name){
        boolean flag = false;
        for (short i = 0; i < noOfContacts; i++) {
            Contact contact = contacts[i];
            if(contact.getName()!= null &&contact.getName().equalsIgnoreCase(name)){
                contact.setName(null);
                contact.setEmail(null);
                contact.setPhoneNumber(0);
                flag = true;
                break;
            }
        }
        if(flag){
            System.out.println("Contact deleted successfully");
        }
        else{
            System.out.println("Details not found");
        }
    }

    public void sortContactsAscendingOrder(){
//        Arrays.sort(contacts, Comparator.nullsLast(Comparator.naturalOrder()));
//        showContacts();
        String[] namesList = new String[noOfContacts];
        for(short i = 0 ; i < noOfContacts; i++){
            Contact contact = contacts[i];
            namesList[i] = contact.getName();
        }
        Arrays.sort(namesList, Comparator.nullsLast(Comparator.naturalOrder()));
        for (short i = 0; i < noOfContacts; i++) {
            for (short j = 0; j < noOfContacts; j++) {
                Contact contact = contacts[j];
                if(contact.getName() != null && contact.getName().equalsIgnoreCase(namesList[i])){
                    System.out.println("Name : " + contact.getName() + " phone number : " + contact.getPhoneNumber() +
                            " Email : " + contact.getEmail());
                }
            }
        }
    }

    boolean validateName(String name){
//        check name is present in phone book or not
        for (short i = 0; i < noOfContacts; i++) {
            Contact _contact = contacts[i];
            if(_contact.getName() != null && _contact.getName().equalsIgnoreCase(name)){
                System.out.println("Given name is already present in contact list");
                return false; }
        }
        return true;
    }

    boolean validatePhoneNumber(long phoneNumber){
        int length = String.valueOf(phoneNumber).length();
        if(length!=10){
            System.out.println("Given mobile number is wrong");
            return false;
        }
        return true;
    }

}

