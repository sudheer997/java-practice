package com.javapartice.oopsconcept;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PhoneBookApp {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PhoneBook phoneBook = new PhoneBook();
        while (true){
            System.out.println("Choose options from below options");
            System.out.println("1.Create new contact");
            System.out.println("2.Modify contact");
            System.out.println("3.Delete contact");
            System.out.println("4.Show Contacts");
            System.out.println("5.View Contact");
            System.out.println("6.Sort contacts in ascending order");
            System.out.println("8.Exit");
            System.out.print("Choose option : ");
            int option;
            try {
                option = Integer.parseInt(br.readLine());
            }
            catch (Exception e){
                System.out.println("Please enter valid number");
                continue;
            }
            if(option == 8) {
                break;
            }

            if(option == 1){
                System.out.print("Enter name : ");
                String name = br.readLine();
                System.out.print("Enter Phone Number : ");
                long phoneNumber = Long.parseLong(br.readLine());
                System.out.print("Enter email address : ");
                String email = br.readLine();
                phoneBook.createContact(name, phoneNumber, email);
            }
            else if(option == 2){
//                modify contact
                System.out.print("Please enter name to modify contact");
                String oldName = br.readLine();
                if(! phoneBook.validateName(oldName)){
                    System.out.println("Please enter new details");
                    System.out.print("Enter name : ");
                    String name = br.readLine();
                    System.out.print("Enter Phone Number : ");
                    long phoneNumber = Long.parseLong(br.readLine());
                    System.out.print("Enter email address : ");
                    String email = br.readLine();
                    phoneBook.modifyContact(oldName, name, phoneNumber, email);
                }
                else{
                    System.out.println("Details Not found");
                }
                /*if(!phoneBook.modifyContact(oldName)){
                    System.out.println("Details Not found");
                }*/
            }
            else if(option == 3){
//                delete new contact
                System.out.print("Please enter name to delete contact : ");
                String name = br.readLine();
                phoneBook.deleteContact(name);
            }
            else if(option == 4){
//               Show Contacts
                phoneBook.showContacts();
            }
            else if(option == 5){
                System.out.print("Enter name to view contact : ");
                phoneBook.showContacts(br.readLine());
            }
            else if(option == 6){
                phoneBook.sortContactsAscendingOrder();
            }
            else{
                System.out.println("Please Enter valid number ");
            }
        }
    }
}
