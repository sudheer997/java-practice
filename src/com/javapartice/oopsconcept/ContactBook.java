package com.javapartice.oopsconcept;

public interface ContactBook {
    void createContact(String name, long phoneNumber, String email);

    void modifyContact(String oldName, String newName, long phoneNumber, String email);

    void showContacts();

    void showContacts(String name);

    void deleteContact(String name);

    void sortContactsAscendingOrder();
}
