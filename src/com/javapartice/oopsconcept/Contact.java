package com.javapartice.oopsconcept;

public class Contact implements Comparable<Contact>{
    private String name;

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private long phoneNumber;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int compareTo(Contact o) {
        /*if(this.name != null && o.getName() != null){
            return this.name.compareTo(o.getName());
        }*/
        if(this.name == null) return 1;
        else if(o.getName() == null) return -1;
        else{
            return this.name.compareTo(o.getName());
        }
    }

    @Override
    public String toString() {
        return "Name : " + getName() + "\nPhone Number : " + getPhoneNumber() + "\nEmail : " + getEmail() ;
    }
}
