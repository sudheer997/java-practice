package com.javapartice.ExceptionHandling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calculator {

    static void validate(short number) throws InvalidNumberException {
        if(!(number >= 0 && number <= 100)){
            throw new InvalidNumberException("Number Should be less than 100 and greater than 0");
        }
    }

    public static void main(String[] args) throws InvalidNumberException, IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            System.out.println("1. Add");
            System.out.println("2. Sub");
            System.out.println("3. Multiplication");
            System.out.println("4. Division");
            System.out.println("5. Power");
            System.out.println("6. Exit");
            System.out.print("Enter your Choice : ");
            short choice = Short.parseShort(br.readLine());
            if(choice == 6){
                break;
            }
            switch (choice){
                case 1 :
                    try {
                        System.out.print("Enter number 1 : ");
                        short number1 = Short.parseShort(br.readLine());
                        validate(number1);
                        System.out.print("Enter number 2 : ");
                        short number2 = Short.parseShort(br.readLine());
                        validate(number2);
                        System.out.println( "Result : "+ (number1 + number2));
                    }
                    catch (InvalidNumberException e){
                        System.err.println(e);
                    }
                    break;
                case 2:
                    try {
                        System.out.print("Enter number 1 : ");
                        short number1 = Short.parseShort(br.readLine());
                        validate(number1);
                        System.out.print("Enter number 2 : ");
                        short number2 = Short.parseShort(br.readLine());
                        validate(number2);
                        System.out.println( "Result : "+ (number1 - number2));
                    }
                    catch (InvalidNumberException e){
                        System.err.println(e);
                    }
                    break;
                case 3:
                    try {
                        System.out.print("Enter number 1 : ");
                        short number1 = Short.parseShort(br.readLine());
                        validate(number1);
                        System.out.print("Enter number 2 : ");
                        short number2 = Short.parseShort(br.readLine());
                        validate(number2);
                        System.out.println( "Result : "+ (number1 * number2));
                    }
                    catch (InvalidNumberException e){
                        System.err.println(e);
                    }
                    break;
                case 4:
                    try {
                        System.out.print("Enter number 1 : ");
                        short number1 = Short.parseShort(br.readLine());
                        validate(number1);
                        System.out.print("Enter number 2 : ");
                        short number2 = Short.parseShort(br.readLine());
                        validate(number2);
                        if(number2 == 0){
                            throw new InvalidNumberException("Number cannot be divided by zero");
                        }
                        System.out.println( "Result : "+ (number1 / number2));
                    }
                    catch (InvalidNumberException e){
                        System.err.println(e);
                    }
                    break;
                case 5:
                    try {
                        System.out.print("Enter number 1 : ");
                        short number1 = Short.parseShort(br.readLine());
                        validate(number1);
                        System.out.print("Enter number 2 : ");
                        short number2 = Short.parseShort(br.readLine());
                        validate(number2);
                        if(number1 == 0 && number2 ==0 ){
                            throw new InvalidNumberException("Zero power zero cannot be defined");
                        }
                        System.out.println( "Result : "+ (Math.pow(number1, number2)));
                    }
                    catch (InvalidNumberException e){
                        System.err.println(e);
                    }
                    break;
                default:
                    System.out.println("Please enter valid number");

            }

        }
    }
}
