package com.javapartice.ExceptionHandling;

public class InvalidNumberException extends Exception{
    InvalidNumberException(String s){
        super(s);
    }
}
